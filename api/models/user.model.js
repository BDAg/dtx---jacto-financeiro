const db = require('../bin/db');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const secret = require('../bin/secret').secret;
const roles = require('../bin/roles');

create = function create(user, result) {
    db.query("insert into USUARIO set ?", user, function (err, res) {

        if (err) {
            result.status(400).send({ error: true, message: err });
        }
        else {
            result.status(200).send({ message: res });
        }
    });
};

find = function find(id, result) {
    db.query("select * from USUARIO where id_user = ? ", id, function (err, res) {
        if (err) {
            result.status(400).send({ error: true, message: err });
        }
        else {
            result.status(200).send({ message: res });
        }
    });
};

login = function login(email, password, result) {
    db.query("select * from USUARIO where email = ? ", email, function (err, res) {
        if (err) {
            result.status(400).send({ error: true, message: err });
        }
        else {
            if (!res) {
                result.status(200).json({
                    'error': null,
                    'response': {
                        'auth': false
                    }
                });
            } else {
                let passwordBD = res[0].senha;
                let response = {
                    'error': null,
                    'response': {
                        'auth': false
                    }
                }

                console.log(passwordBD);

                const isAuth = bcrypt.compareSync(password, passwordBD);
                
                if (isAuth) {
                    let token = jwt.sign({ 'role': res[0].role }, secret);
                    response['response']['auth'] = isAuth;
                    response['response']['token'] = token;
                    response['response']['role'] = res[0].role;
                    response['response']['id_user'] = res[0].id_user;
                    result.status(201).json(response);

                } else {
                    result.status(406).json(response);
                }
            }
        }
    });
};

findAll = function findAll(result) {
    db.query("select * from USUARIO where role = 1", function (err, res) {

        if (err) {
            result.status(400).send({ error: true, message: err });
        }
        else {
            result.status(200).send({ message: res });
        }
    });
};

update = function update(id, user, result) {
    db.query("update USUARIO set ? where id_user = ?", [user, id], function (err, res) {
        if (err) {
            result.status(400).send({ error: true, message: err });
        }
        else {
            result.status(200).send({ message: res });
        }
    });
};

remove = function (id, result) {
    db.query("delete from USUARIO where id_user = ?", [id], function (err, res) {

        if (err) {
            result.status(400).send({ error: true, message: err });
        }
        else {
            result.status(200).send({ message: res });
        }
    });
};

module.exports = {
    create,
    find,
    login,
    findAll,
    remove,
    update
}