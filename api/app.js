var express = require('express');
var bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');

app = express();

app.use(cors());
app.use(express.json());

app.use(morgan('dev'));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var userRoutes = require('./routes/user.router');
app.use('/user', userRoutes);


var port = '3000';

app.set('port', process.env.PORT || port);
app.listen(app.get('port'), () => {
    console.log(`server on port ${app.get('port')}`);
});

module.exports = app;