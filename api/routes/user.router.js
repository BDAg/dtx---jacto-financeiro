const express = require('express');
const router = express.Router();

const authorize = require('../bin/authorize');
const roles = require('../bin/roles');

var userController = require('../controllers/user.controller');

router.get('/', authorize([roles.admin]), userController.getAll);

router.get('/get/:id', authorize([roles.user, roles.admin]), userController.get);

router.post('/register', userController.post);

router.get('/delete/:id', userController.get);

router.post('/login', userController.login);

module.exports = router;