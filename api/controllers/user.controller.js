const bcrypt = require('bcryptjs');
const Usuarios = require('../models/user.model');

const secret = require('../bin/secret').secret;

get = function (req, res) {
    const id = req.params.id;

    if (!id) {
        return res.status(400).send({ error: true, message: 'Id não informado' });
    }

    Usuarios.find(id, res);

};

getAll = function (req, res) {
    Usuarios.findAll(res)
}

post = function (req, res) {
    let user = req.body;

    if (!user) {
        return res.status(400).send({ error: true, message: 'Objeto não informado' });
    }

    let newPassword = bcrypt.hashSync(user['senha'], 10);

    user['senha'] = newPassword;

    (!user['role']) ? user['role'] = 1 : pass;

    Usuarios.create(user, res);
};

put = function (req, res) {
    let user = req.body;

    if (!user) {
        return res.status(400).send({ error: user, message: 'Objeto não informado' });
    }

    Usuarios.update(user, res);
};

remove = function (req, res) {

    let id = req.params.id;

    if (!id) {
        return res.status(400).send({ error: true, message: 'Id não informado' });
    }

    Usuarios.remove(id, res);

}

login = function (req, res) {

    let email = req.body.email;
    let senha = req.body.senha;

    if (!email || !senha) {
        return res.status(400).send({ error: true, message: 'Senha ou e-mail incorretos' });
    }

    Usuarios.login(email, senha, res);

}

module.exports = {
    get,
    getAll,
    login,
    post,
    put,
    remove
}