import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { User } from 'src/model/user';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

// const apiUrl = 'http://localhost:8080/user';
const apiUrl = 'https://leancreditapi.herokuapp.com/user';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getToken() {
    return localStorage.getItem('token');
  }

  getHeaders() {
    return {
      headers: new HttpHeaders({
        Accept: 'application/json',
        Authorization: `Bearer ${ this.getToken() }`
      })
    };
  }

  getUsers() {
    return this.http.get(apiUrl, this.getHeaders());
  }

  getUser(id: number) {
    return this.http.get(`${apiUrl}/get/${id}`, this.getHeaders());
  }

  addUser(user) {
    return this.http.post(`${apiUrl}/register`, user, httpOptions);
  }

  login(form: {email: string, senha: string}) {
    return this.http.post(`${apiUrl}/login`, form);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error);

      return of(result as T);
    };
  }
}
