import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(private apiService: ApiService, private activatedRouter: ActivatedRoute) { }

  idUser: string;
  user: any[] = [];

  ngOnInit() {
    this.getOnlyUser(this.activatedRouter.snapshot.paramMap.get('id'));
  }

  getOnlyUser(id) {
    this.apiService.getUser(id).subscribe(
      res => {
        // tslint:disable-next-line: no-string-literal
        this.user = res['message'];
        // console.log('users', this.user);
      },
      err => {
        console.log('err', err);
      }
    );
  }
}
