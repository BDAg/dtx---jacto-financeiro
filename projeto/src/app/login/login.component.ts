import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;

  constructor(private apiService: ApiService, private formBuilder: FormBuilder, private router: Router) {}

  ngOnInit() {
    this.form = this.formBuilder.group({
      email: null,
      senha: null
    });
  }

  login() {
    this.apiService.login(this.form.value).subscribe(
      res => {
        // tslint:disable-next-line: no-string-literal
        localStorage.setItem('token', res['response']['token']);

        // console.log(res);

        // tslint:disable-next-line: no-string-literal
        if (res['response']['role'] === 2) {
          this.router.navigate(['/admin']);
        } else {
          // tslint:disable-next-line: no-string-literal
          this.router.navigate([`/user/${ res['response']['id_user'] }`]);
        }
        // tslint:disable-next-line: no-string-literal
        // console.log('token', res['response']);
      },
      err => {
        console.log(err);
      }
    );
  }

}
