import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { ApiService } from '../api.service';

@Component({
    selector: 'app-cadastrar',
    templateUrl: './cadastrar.component.html',
    styleUrls: ['./cadastrar.component.css']
})

export class CadastrarComponent implements OnInit {
    formPrincipal: FormGroup;
    confirmPassword: boolean;
    selectedEstado: string;
    identifi: string;
    confirmSubmit = false;
    disableTextbox = false;

    masksuframa = '000000000';
    maskidentifi = '000.000.000-09';


    masksSuframa = {
        AC: '000000000',
        AL: '000000000',
        AM: '000.000.000.000',
        AP: '00.000.0000-0',
        BA: '000000-00',
        CE: '00000000-0',
        DF: '00000000000-00',
        ES: '00000000-0',
        GO: '00.000.000-0',
        MA: '00000000-0',
        MT: '0000000000-0',
        MS: '00000000-0',
        MG: '000.000.000/0000',
        PA: '00-000000-0',
        PB: '00000000-0',
        PR: '000.00000-00',
        PE: '0000000-00',
        PI: '00000000-0',
        RJ: '00.000.00-0',
        RN: '00.000.000-0',
        RS: '000/0000000',
        RO: '0000000000000-0',
        RR: '00000000-0',
        SP: '000.000.000.000',
        SC: '000.000.000',
        SE: '00000000-0',
        TO: '0000000000-0',
    };

    masksIdentifi = { CNPJ: '00.000.000/0000-00', CPF: '000.000.000-09' };

    constructor(private apiService: ApiService, private formBuilder: FormBuilder) { }

    ngOnInit() {
        this.formPrincipal = this.formBuilder.group({
            nome: null,
            email: null,
            telefone: null,
            senha: null,
            identificacao: null,
            tipo_cliente: null,
            suframa: null,
            insc_rural: null,
            insc_estadual: null,
            endereco: null,
            estado: 'SP',
            cep: null,
            tipoIdentificacao: null,
            suframaCheck: null,
            confPass: null
        });

        this.formPrincipal.get('suframa').disable();
    }

    toggleDisable() {
        if (this.disableTextbox === false) {
            this.disableTextbox = !this.disableTextbox;
            this.formPrincipal.get('suframa').enable();
        } else {
            this.disableTextbox = !this.disableTextbox;
            this.formPrincipal.get('suframa').disable();
        }
    }

    changemasksuframa() {
        this.masksuframa = this.masksSuframa[this.selectedEstado];
    }

    changemaskIdentifi() {
        this.maskidentifi = this.masksIdentifi[this.identifi];
    }


    checkPassSame() {
        const password = this.formPrincipal.value.senha;
        const passConf = this.formPrincipal.value.confPass;
        if (password === passConf) {
            this.confirmPassword = false;
            return this.confirmPassword;
        } else {
            this.confirmPassword = true;
            return this.confirmPassword;
        }
    }


    onSubmit() {
        if (this.confirmPassword) {
            console.log('error');
            return false;
        } else {
            this.confirmSubmit = true;
            console.log('success');
        }

        const {
            nome,
            email,
            telefone,
            senha,
            identificacao,
            tipo_cliente,
            suframa,
            insc_rural,
            insc_estadual,
            endereco,
            estado,
            cep,
            tipoIdentificacao
        } = this.formPrincipal.value;

        const form = {
            nome,
            email,
            telefone,
            senha,
            identificacao,
            tipo_cliente,
            suframa,
            insc_rural,
            insc_estadual,
            endereco,
            estado,
            cep,
            tipoIdentificacao
        };

        this.apiService.addUser(form).subscribe(
            res => {
                // console.log('res addUser', res);
                this.formPrincipal.reset();
            },
            err => {
                console.log('err addUser', err);
            }
        );
    }
}
