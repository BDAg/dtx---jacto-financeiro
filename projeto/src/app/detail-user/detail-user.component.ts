import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-detail-user',
  templateUrl: './detail-user.component.html',
  styleUrls: ['./detail-user.component.css']
})
export class DetailUserComponent implements OnInit {

  constructor(private apiService: ApiService, private activatedRoute: ActivatedRoute) { }

  idUser: string;
  user: any[] = [];
  aceData = {};
  aceService = 'http://localhost:3000';
  aceCatcher: any;
  loading = false;

  ngOnInit() {
    this.idUser = this.activatedRoute.snapshot.paramMap.get('id');

    this.getOnlyUser(this.idUser).subscribe(
      res => {
        // tslint:disable-next-line: no-string-literal
        this.user = res['message'];
        this.getScore(this.user[0].identificacao);
      },
      err => {
        console.log('err', err);
      }
    );

    // fetch(`${this.aceService}/${this.user[0].identificacao}`).then(
    //   // tslint:disable-next-line: no-unused-expression
    //   res => {
    //     console.log(res);
    //   },
    //   err => {
    //     console.log(err);
    //   });
  }

  getOnlyUser(id) {
    return this.apiService.getUser(id);
  }

  getScore(cpf) {
    this.loading = true;
    fetch(`${this.aceService}/`,
    {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'POST',
      body: JSON.stringify({ cpf })
    }).then(
    // tslint:disable-next-line: no-unused-expression
    res => {
      this.loading = false;
      res.json().then(
        value => {
          this.aceData = value;
        }
      );
    },
    err => {
      this.loading = false;
      console.log(err);
    });
  }

}
