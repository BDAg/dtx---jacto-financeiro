import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  form: FormGroup;
  users: any[] = [];

  constructor(private apiService: ApiService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.listUsers();

    this.form = this.formBuilder.group({
      search: ''
    });
  }

  get filtredUsers() {
    return this.users.filter(user => user.nome.toLowerCase().includes(this.form.get('search').value.toLowerCase()));
  }

  listUsers() {
    this.apiService.getUsers().subscribe(
      res => {
        // console.log('res', res);
        // tslint:disable-next-line: no-string-literal
        this.users = res['message'];
      },
      err => {
        console.log('err', err);
      }
    );
  }

  detailedUser(id) {
    this.router.navigate(['/detail-user', id]);
  }

}
