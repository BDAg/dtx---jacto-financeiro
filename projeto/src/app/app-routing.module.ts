import { NgModule } from '@angular/core';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { CadastrarComponent } from './cadastrar/cadastrar.component';
import { LoginComponent } from './login/login.component';
import { AdminComponent } from './admin/admin.component';
import { UserComponent } from './user/user.component';
import { DetailUserComponent } from './detail-user/detail-user.component';


const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'cadastrar', component: CadastrarComponent },
    { path: 'login', component: LoginComponent },
    { path: 'admin', component: AdminComponent },
    { path: 'user/:id', component: UserComponent },
    { path: 'detail-user/:id', component: DetailUserComponent },
    { path: '**', pathMatch: 'full', redirectTo: ''},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
