export class User {
    // tslint:disable-next-line: variable-name
    id_user: string;
    nome: string;
    email: string;
    telefone: string;
    senha: string;
    cpf: string;
    cnpj: string;
    // tslint:disable-next-line: variable-name
    tipo_cliente: string;
    suframa: string;
    // tslint:disable-next-line: variable-name
    insc_rural: string;
    // tslint:disable-next-line: variable-name
    insc_estadual: string;
    endereco: string;
    estado: string;
    role: number;
    cep: string;
    tipoIdentificacao: string;
}
